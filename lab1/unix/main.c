#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

int ti[] = {0, 0};
typedef struct file_info_s
{
	int		*turn;
	int 	is_used;
} file_info;

typedef struct thread_data_s
{
	int				id;
	file_info		*file;
	int				is_priority;
} thread_data;

void thread_log(int id, int priority, char* string)
{
	printf("thread %d {%d}> %s\n", id, priority, string);
}

void dekker_set(file_info file, int id, int flag)
{
	file.turn[id] = flag;
}

int  dekker_other(file_info file, int id)
{
	if (id == 0)
		return (file.turn[1] == 1);
	if (id == 1)
		return (file.turn[0] == 1);
	return (0);
}

void dekker_in(file_info *file)
{
	file->is_used = 1;
}

void dekker_out(file_info *file)
{
	file->is_used = 0;
}

int dekker_open(file_info *file, int id, int is_priority, void (*action)(int, int))
{
	while(ti[id] != 1)
		usleep(10);
	thread_log(id, is_priority, "Установка флага");
	dekker_set(*file, id, 1);
	while(ti[id] == 1)
	{
		while(ti[id] != 2)
			usleep(10);
		if (dekker_other(*file, id) == 0)
		{
			thread_log(id, is_priority, "Другой процесс НЕ намерен войти в критический участок");
			dekker_in(file);
			thread_log(id, is_priority, "Вход в критический участок");
			action(id, is_priority);
			while(ti[id] != 3)
				usleep(10);
			thread_log(id, is_priority, "Выход из критического участка");
			dekker_out(file);
			while(ti[id] != 4)
				usleep(10);
			thread_log(id, is_priority, "Снятие флага");
			dekker_set(*file, id, 0);
			return (1);
		}
		thread_log(id, is_priority, "Другой процесс намерен войти в критический участок");
		if (file->is_used == 0)
			thread_log(id, is_priority, "Файл НЕ используется");
		else
			thread_log(id, is_priority, "Файл используется");
		if (is_priority && file->is_used == 0)
		{
			thread_log(id, is_priority, "Вход в критический участок");
			dekker_in(file);
			action(id, is_priority);
			while(ti[id] != 3)
				usleep(10);
			thread_log(id, is_priority, "Выход из критического участка");
			dekker_out(file);
			while(ti[id] != 4)
				usleep(10);
			thread_log(id, is_priority, "Снятие флага");
			dekker_set(*file, id, 0);
			return (1);
		}
		else
		{
			thread_log(id, is_priority, "Ожидание освобождения");
			--ti[id];
		}
	}
	return (0);
}

void do_some_action(int id, int is_priority)
{
	thread_log(id, is_priority, "...");
}

void *th(void *vargp)
{
	thread_data *data = (thread_data *)vargp;
	dekker_open(data->file, data->id, data->is_priority, &do_some_action);
	return (NULL);
}

int main()
{
	pthread_t th0_id;
	pthread_t th1_id;
	int turn[] = {0, 0};
	int n;
	file_info file = {turn, 0};
	thread_data th_data0 = {0, &file, 1};
	thread_data th_data1 = {1, &file, 0};

	pthread_create(&th0_id, NULL, th, (void *)&th_data0);
	pthread_create(&th1_id, NULL, th, (void *)&th_data1);
	while (1)
	{
		usleep(35);
		printf(">> ");
		scanf("%d", &n);
		if (n == 0 || n == 1)
			ti[n] += 1;
		else
			break;
	}
}
